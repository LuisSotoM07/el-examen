﻿using System;

namespace S.G.C.V
{
    class Program
    {
        static void Main(string[] args)
        {
            int precio;
            byte opcion_pago;
            string num_tarjeta;

            try
            {
                Console.WriteLine("Bienvenido usuario, digite el precio del producto");
                precio = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                Console.WriteLine("Ahora digite la forma de pago");
                pago();
                opcion_pago = Convert.ToByte(Console.ReadLine());

                if (opcion_pago == 1)
                {
                    Console.WriteLine("PAGO REALIZADO, PRESIONE ENTER PARA SALIR");
                    Console.ReadKey();
                }
                else if (opcion_pago == 2)
                {
                    Console.WriteLine("INGRESE EL NUMERO DE TARJETA");
                    num_tarjeta = Console.ReadLine();
                    Console.WriteLine("PAGO REALIZADO, PRESIONE ENTER PARA SALIR");
                    Console.ReadKey();
                }


            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
            }
        }
        static void pago()
        {
            Console.WriteLine("1 - EFECTIVO");
            Console.WriteLine("2 - TARJETA");

        }
    }
}